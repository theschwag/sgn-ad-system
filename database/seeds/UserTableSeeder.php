<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;

class UserTableSeeder extends Seeder
{

public function run()
{
    DB::table('users')->delete();
    User::create(array(
        'name'     => 'Pete Schwager',
        'email'    => 'hello@peteschwager.com',
        'role_id' => 3,
        'customer_id' => '',
        'password' => Hash::make('Staci1029'),
    ));
}

}
