<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Role;

class RoleTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('roles')->delete();
        Role::create([
            'id'            => 1,
            'name'          => 'standard',
            'description'   => 'Standard access. Can view info only about their account.'
        ]);
        Role::create([
            'id'            => 2,
            'name'          => 'admin',
            'description'   => 'Full access to create, edit, update, and delete'
        ]);
        Role::create([
            'id'            => 3,
            'name'          => 'superadmin',
            'description'   => 'Same as Admin but with ability to see deleted information and restore it.'
        ]);
    }
}
