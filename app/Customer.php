<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Customer extends Model
{
    use SoftDeletes;

    protected $table = 'customers';
    protected $fillable = [
        'name',
        'email',
        'address',
        'city',
        'state',
        'phone'
    ];
    protected $dates = ['deleted_at'];

}
