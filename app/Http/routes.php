<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'PageController@index');
Route::get('dashboard', 'DashboardController@index');

// Password reset link request routes
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::group(['middleware' => ['auth', 'roles'], 'roles' => ['admin', 'superadmin'], 'protect' => true], function () {
    // protected user routes
    Route::get('users', 'UserController@index');
    Route::get('users/create', 'UserController@create');
    Route::post('users', 'UserController@store');
    Route::delete('users/{id}', 'UserController@destroy');
    Route::post('users/{id}', 'UserController@restore');
    // protected customers routes
    Route::resource('customers', 'CustomerController');
    Route::post('customers/{id}', 'CustomerController@restore');
    // protected ads routes
    Route::post('ads/{id}', 'AdController@restore');
    // protected messages routes
    Route::resource('messages', 'MessageController');
    Route::post('messages/{id}', 'MessageController@restore');
    //protect all ad routes except index
    Route::resource('ads', 'AdController');
});

// allow users to edit their profile password, but limit other capabilities
Route::get('users/{id}/edit', [
    'middleware' => ['auth', 'roles'],
    'uses' => 'UserController@edit',
    'roles' => ['standard', 'admin', 'superadmin']
]);
Route::patch('users/{id}', [
    'middleware' => ['auth', 'roles'],
    'uses' => 'UserController@update',
    'roles' => ['standard', 'admin', 'superadmin']
]);

// allow users to ads index, view will vary by role and customer
Route::get('ads', 'AdController@index');

Route::post('login', 'Auth\AuthController@login');
Route::controller('/', 'Auth\AuthController');
