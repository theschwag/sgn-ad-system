<?php

namespace App\Http\Controllers;

use App\Message;
use Auth;

class DashboardController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index()
    {
        // get role of current user
        $current_role = Auth::user()->role_id;

        switch ($current_role) {
            case 3: // If superadmin show all messages, even deleted ones
                $messages = Message::withTrashed()->orderBy('created_at', 'desc')->get();
                break;
            case 2: // if admin only show active messages
                $messages = Message::latest()->orderBy('created_at', 'desc')->get();
                break;
            case 1:
                $messages = Message::latest()->where('role_id', $current_role)->orWhere('role_id', 4)->orderBy('created_at', 'desc')->get();
                break;
        }

        $data = [
            'title' => 'Dashboard',
            'heading' => 'Dashboard',
            'messages' => $messages
        ];
        return view('dashboard.index', $data);
    }

}
