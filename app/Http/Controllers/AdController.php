<?php namespace App\Http\Controllers;

use App\Ad;

use Mail;
use DB;
use Carbon;
use Auth;
use UrlShortener;

use App\Http\Requests;
use App\Http\Requests\AdRequest;
use Input;
use Session;

class AdController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index()
    {

        // get role of current user
        $current_role = Auth::user()->role_id;

        switch ($current_role) {
            case 3: // If superadmin show all ads, even deleted ones
                $ads = Ad::GetAllAds();
                $heading = 'All Ads';
                break;
            case 2: // if admin only show active ads
                $ads = Ad::GetCustomer();
                $heading = 'Active Ads';
                break;
            case 1:
                $customerid = Auth::user()->customer_id;
                $ads = Ad::GetMyAds()->where('customers.id', $customerid)->get();;
                $heading = 'My Active Ads';
        }

        $data = [
            'title' => 'Ads',
            'heading' => $heading,
            'ads' => $ads
        ];

        return view('ads.index', $data);
    }

    public function create()
    {
        // get customer list from db for dropdown in the ad create form
        $customer_list = array('' => 'Select a customer') + DB::table('customers')->where('deleted_at', null)->orderBy('name', 'asc')->lists('name', 'id');

        $data = [
            'title' => 'Create New Ad',
            'heading' => 'Create New Ad',
            'customer_list' => $customer_list
        ];

        return view('ads.create', $data);
    }

    public function store(AdRequest $request)
    {
        //add file upload to storage/ads folder
        if ($request->file('image_link')):
            $filename = explode('.', $request->file('image_link')->getClientOriginalName());
            $newfilename = $filename[0] . '_' . Carbon::now()->timestamp . '.' . $filename[1];
            Input::file('image_link')->move('../public/images/ads/', $newfilename);
            $request->instance()->query->set('ad_file', $newfilename);
        endif;

        // re-format date for db columns
        $converted_startdate = Carbon::parse($request->input('start_date'))->toDateString();
        $converted_enddate = Carbon::parse($request->input('end_date'))->toDateString();
        $request->merge(array(
            'start_date' => $converted_startdate,
            'end_date' => $converted_enddate
        ));

        // if url, get it and then turn into trackable goo.gl url
        $submitted_url = $request->input('url_to');
        $utm = '?utm_campaign=sgn_blog&utm_content=posted_' . Carbon::now()->timestamp . '&utm_medium=banner_ad&utm_source=sgnscoops.com';
        if ($submitted_url):
            $submitted_url .= $utm;
            $trackthis = UrlShortener::driver('bitly')->shorten($submitted_url);
            $secure_track = preg_replace("/^http:/i", "https:", $trackthis);
            $request->instance()->query->set('utm', $utm);
            $request->instance()->query->set('tracking_url', $secure_track);
        endif;

        // if passes validation
        Ad::create($request->all());

        $getstart = Carbon::parse($request->input('start_date'));
        $getend = Carbon::parse($request->input('end_date'));

        // send email to customer
        $data = DB::table('customers')->select('name', 'email')->where('id', $request->input('customer_id'))->get();
        $adinfo = [
            'start' => $getstart->format('m/d/Y'),
            'end' => $getend->format('m/d/Y')
        ];

        Mail::send('emails.adscheduled', $adinfo, function ($m) use ($data) {
            $m->from('rob@sgnscoops.com', 'SGN Scoops Web Ad System');
            $m->to($data[0]->email, $data[0]->name);
            $m->subject('SGN Scoops web ad has been scheduled');
        });

        Session::flash('message', 'Ad created successfully!');

        return redirect('ads');

    }

    public function edit($id)
    {

        if (Ad::find($id) == null) {
            return view('errors.noaccess', ['title' => 'Edit Ad', 'heading' => 'Edit Ad']);
        }

        $ad = Ad::findOrFail($id);
        $customer_list = array('' => 'Select a customer') + DB::table('customers')->orderBy('name', 'asc')->lists('name', 'id');

        $data = [
            'title' => 'Edit Ad',
            'heading' => 'Edit Ad',
            'customer_list' => $customer_list,
            'ad' => $ad
        ];

        return view('ads.edit', $data);

    }

    public function update(AdRequest $request, $id)
    {
        $ad = Ad::findOrFail($id);

        //add file upload to storage/ads folder
        if ($request->file('image_link')):
            $filename = explode('.', $request->file('image_link')->getClientOriginalName());
            $newfilename = $filename[0] . '_' . Carbon::now()->timestamp . '.' . $filename[1];
            Input::file('image_link')->move('../public/images/ads/', $newfilename);
            $request->instance()->query->set('ad_file', $newfilename);
        endif;

        // if url, get it and then turn into trackable goo.gl url
        $submitted_url = $request->input('url_to');
        $utm = '?utm_campaign=sgn_blog&utm_content=posted_' . Carbon::now()->timestamp . '&utm_medium=banner_ad&utm_source=sgnscoops.com';
        if ($submitted_url):
            $submitted_url .= $utm;
            $trackthis = UrlShortener::driver('bitly')->shorten($submitted_url);
            $request->instance()->query->set('utm', $utm);
            $request->instance()->query->set('tracking_url', $trackthis);
        endif;

        // re-format date for db columns
        $converted_startdate = Carbon::parse($request->input('start_date'))->toDateString();
        $converted_enddate = Carbon::parse($request->input('end_date'))->toDateString();
        $request->merge(array(
            'start_date' => $converted_startdate,
            'end_date' => $converted_enddate
        ));

        // if passes validation
        $ad->update($request->all());
        Session::flash('message', 'Ad updated successfully!');

        return redirect('ads');
    }

    public function destroy($id)
    {
        $ad = Ad::find($id);
        $ad->delete();

        // redirect
        Session::flash('message', 'Ad has been deleted!');
        return redirect('ads');
    }

    public function restore($id)
    {
        $ad = Ad::withTrashed()->find($id);
        $ad->restore();

        Session::flash('message', 'Ad restored!');
        return redirect('ads');
    }

}
