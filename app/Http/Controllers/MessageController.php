<?php namespace App\Http\Controllers;

use App\Message;

use App\Http\Requests;
use App\Http\Requests\MessageRequest;
use Input;
use Session;

use App\Http\Controllers\Controller;

class MessageController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $data = [
        'title' => 'Post Message',
        'heading' => 'Post New Message'
      ];

      return view('messages.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MessageRequest $request)
    {
      Message::create($request->all());
      Session::flash('message', 'Message posted!');

      return redirect('dashboard');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
     {
         $message = Message::find($id);
         $message->delete();

         // redirect
         Session::flash('message', 'Message deleted!');
         return redirect('dashboard');
     }

     public function restore($id)
     {

        $message = Message::withTrashed()->find($id);
        $message->restore();

        Session::flash('message', 'Message restored!');
        return redirect('dashboard');
     }
}
