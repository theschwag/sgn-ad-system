<?php namespace App\Http\Controllers;

use App\User;
//use DB;
use Hash;
use Auth;
use App\Http\Requests;
use App\Http\Requests\UserRequest;
use Input;
use Session;
use Route;
use Mail;

use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        // get role of current user
        $current_role = Auth::user()->role_id;

        switch ($current_role) {
            case 3: // If superadmin show all users, even deleted ones
                $users = User::withTrashed()->get();
                $heading = 'All Users';
                break;
            case 2: // if admin only show admin users and down, no deleted ones
                $users = User::latest()->where('deleted_at', null)->where('role_id', '<', 3)->get();
                $heading = 'Active Users';
                break;
        }


        $data = [
            'title' => 'Users',
            'heading' => $heading,
            'users' => $users
        ];

        return view('users.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'title' => 'Create New User',
            'heading' => 'Create New User'
        ];

        return view('users.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {

        $userexists = User::where('email', '=', Input::get('email'))->exists();
        if ($userexists) {

            Session::flash('error', 'A user already exists with that email');
            return redirect('users/create')->withInput();

        } else {

            $hashpass = $request->input('password');
            $request->merge(array(
                'password' => Hash::make($hashpass)
            ));

            User::create($request->all());
            Session::flash('message', 'User created successfully!');

            return redirect('users');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $current_role = Auth::user()->role_id;
        $current_user_id = Auth::user()->id;

        $user = User::findOrFail($id);

        $data = [
            'title' => 'Edit User',
            'heading' => 'Edit User',
            'user' => $user
        ];

        if($current_role == 1) {
            if($id != $current_user_id) {
                return view('errors.noaccess', ['title' => 'Edit User', 'heading' => 'No Access']);
            } else {
                return view('users.edit', $data);
            }
        } else {
            return view('users.edit', $data);
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {

        $user = User::findOrFail($id);
        // if passes validation
        //$checkemail = $request->input('email');
        $newpass = $request->input('update_password');
        //if($checkemail == $user->email):
        if ($newpass):
            $request->merge(array(
                'password' => Hash::make($newpass)
            ));
        endif;

        $user->update($request->all());
        Session::flash('message', 'User updated successfully!');

        $current_role = Auth::user()->role_id;

        if($current_role == 1){
            Session::flash('message', 'Password updated');
            return redirect('dashboard');
        } else {
            return redirect('users');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        // redirect
        Session::flash('message', 'User deleted!');
        return redirect('users');
    }

    /**
     * Restore the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $user = User::withTrashed()->find($id);
        $user->restore();

        Session::flash('message', 'User restored!');
        return redirect('users');
    }
}
