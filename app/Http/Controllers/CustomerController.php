<?php namespace App\Http\Controllers;

use App\Customer;
use App\User;

//use DB;
use Hash;
use Auth;
use App\Http\Requests;
use App\Http\Requests\CustomerRequest;
use Input;
use Session;
use Mail;

use App\Http\Controllers\Controller;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // get role of current user
        $current_role = Auth::user()->role_id;

        switch ($current_role) {
            case 3: // If superadmin show all users, even deleted ones
                $customers = Customer::withTrashed()->get();
                $heading = 'All Customers';
                break;
            case 2: // if admin only show admin users and down, no deleted ones
                $customers = Customer::latest()->get()->where('deleted_at', null);
                $heading = 'Active Customers';
                break;
        }

        $data = [
            'title' => 'Customers',
            'heading' => $heading,
            'customers' => $customers
        ];
        return view('customers.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'title' => 'Create New Customer',
            'heading' => 'Create New Customer'
        ];

        return view('customers.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerRequest $request)
    {
        $userexists = User::where('email', '=', Input::get('email'))->exists();
        $customerexists = Customer::where('email', '=', Input::get('email'))->exists();
        if($userexists || $customerexists){
            Session::flash('error', 'A user already exists with that email');
            return redirect('customers/create')->withInput();
        } else {
            $customer = Customer::create($request->all());

            // create standard user account based on new customer profile
            $user = new User;
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->password = Hash::make('password');
            $user->customer_id = $customer->id;
            $user->role_id = '1';
            Session::flash('message', 'Customer created successfully!');
            $user->save(); // save info to users table

            // send email to let user know about new account
            $userlink = User::find($user->id);
            $info = [
              'link' => $userlink
            ];

            Mail::send('emails.usercreated', $info, function ($m) use ($user) {
                $m->from('rob@sgnscoops.com', 'SGN Scoops Web Ad System');
                $m->to($user->email, $user->name);
                $m->subject('New user account created');
            });

            return redirect('customers');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        if (Customer::find($id) == null) {
            return view('errors.noaccess', ['title' => 'Edit Customer', 'heading' => 'Edit Customer']);
        }

        $customer = Customer::findOrFail($id);

        $data = [
            'title' => 'Edit Customer',
            'heading' => 'Edit Customer',
            'customer' => $customer
        ];

        return view('customers.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CustomerRequest $request, $id)
    {

        $customer = Customer::findOrFail($id);
        // if passes validation
        $customer->update($request->all());
        Session::flash('message', 'Customer updated successfully!');

        return redirect('customers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::find($id);
        $customer->delete();

        // redirect
        Session::flash('message', 'Customer deleted!');
        return redirect('customers');
    }

    public function restore($id)
    {

        $customer = Customer::withTrashed()->find($id);
        $customer->restore();

        Session::flash('message', 'Customer restored!');
        return redirect('customers');
    }
}
