<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Message extends Model
{
  use SoftDeletes;

  protected $table = 'messages';
  protected $fillable = [
    'message',
    'role_id'
  ];
  protected $dates = ['deleted_at'];

}
