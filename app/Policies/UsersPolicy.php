<?php

use Bausch\LaravelFortress\Contracts\FortressPolicy;

class UsersPolicy implements FortressPolicy
{
    /**
     * Fortress Roles.
     *
     * @return array
     */
    public function fortress_roles()
    {
        return [
            'admin' => [
                'create',
                'read'
                'edit',
                'update',
                'destroy',
            ],
        ];
    }
  }
