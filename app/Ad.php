<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Ad extends Model
{
    use SoftDeletes;

    protected $table = 'ads';
    protected $fillable = [
        'customer_id',
        'url_to',
        'tracking_url',
        'utm',
        'ad_file',
        'position',
        'start_date',
        'end_date',
        'active'
    ];
    protected $dates = ['deleted_at'];

    public function scopeGetCustomer()
    {

        $customer = DB::table('ads')
            ->join('customers', 'ads.customer_id', '=', 'customers.id')
            ->select('ads.*', 'customers.name', 'customers.deleted_at as cust_deleted')
            ->where('ads.deleted_at', null)
            ->where('customers.deleted_at', null)
            ->get();

        return $customer;

    }

    public function scopeGetMyAds($customerid)
    {

        $myads = DB::table('ads')
            ->join('customers', 'ads.customer_id', '=', 'customers.id')
            ->select('ads.*', 'customers.name', 'customers.deleted_at as cust_deleted', 'customers.id')
            ->whereNull('ads.deleted_at')
            ->whereNull('customers.deleted_at');

        return $myads;

    }

    public function scopeGetAllAds()
    {
        $allads = DB::table('ads')
            ->join('customers', 'ads.customer_id', '=', 'customers.id')
            ->select('ads.*', 'customers.name', 'customers.deleted_at as cust_deleted')
            ->whereNotNull('ads.deleted_at')
            ->orWhere('ads.deleted_at', null)
            ->get();

        return $allads;

    }
}
