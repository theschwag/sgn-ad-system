<table>
  <tr>
    <td>
      This is just a notification to let you know your ad on sgnscoops.com has been scheduled.<br>
      Thank you!
    </td>
  </tr>
  <tr>
    <td>
      <br>
      <br>
      Ad will start on: <strong>{{ $start }}</strong><br>
      Ad will end on: <strong>{{ $end }}</strong>
    </td>
  </tr>
</table>
