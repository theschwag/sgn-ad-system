<table>
    <tr>
        <td>
            A user account has been created for you on SGN Scoops Ad System<br>
            <a href="{{ $link }}">Click here</a> to login for the first time.
        </td>
    </tr>
    <tr>
        <td>
            <br>
            After logging in, you can click your name on the top right and change your password to something you can remember.
        </td>
    </tr>
</table>