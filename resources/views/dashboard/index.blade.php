@extends('content')

@section('inner')

    @foreach($messages as $msg)
        <div class="well padding1 margin_btm1 <?php echo ($msg->deleted_at != NULL) ? 'deleted' : ''; ?>">
            <div class="bold italic">
                <div class="row margin0">
                    <?php $posted_date = Carbon::parse($msg->created_at); ?>
                    <div class="col-sm-6 padding0">
                        <span class="float_left">Posted <?php echo $posted_date->diffForHumans(); ?></span>
                    </div>
                    <div class="col-sm-6 padding0">
                        <span class="float_right">
                          <?php
                            switch ($msg->role_id) {
                                case 1:
                                    echo '<span class="text-info"><i class="fa fa-user"></i>&nbsp;Sent to Customers</span>';
                                    break;
                                case 2:
                                    echo '<span class="text-info"><i class="fa fa-lock"></i>&nbsp;Sent to Admins</span>';
                                    break;
                                case 4:
                                    echo '<span class="text-info"><i class="fa fa-users"></i>&nbsp;Sent to Everyone</span>';
                                    break;
                            }
                            ?>
                        </span>
                    </div>
                </div>
                <div class="row margin0">
                    <hr style="margin-top:0;">
                </div>
            </div>
            <div>{{ $msg->message }}</div>
            @if(Auth::user()->role_id != 1)
            <div class="row padding_lr1">
                <div class="pull-right clearfix">
                    @if($msg->deleted_at == NULL)
                        {!! Form::open(['method' => 'DELETE', 'id' => 'DeleteMessage', 'class' => 'inline', 'action' => ['MessageController@destroy', $msg->id]]) !!}
                        {!! Form::button( '<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-fab margin0 btn-danger delete'] ) !!}
                        {!! Form::close() !!}
                    @endif
                    @if($msg->deleted_at != NULL)
                        {!! Form::open(['method' => 'RESTORE', 'id' => 'RestoreMessage', 'class' => 'inline', 'action' => ['MessageController@restore', $msg->id]]) !!}
                        {!! Form::button( '<i class="fa fa-undo"></i>', ['type' => 'submit', 'class' => 'btn btn-fab margin0 btn-success restore'] ) !!}
                        {!! Form::close() !!}
                    @endif
                </div>
            </div>
            @endif
        </div>
    @endforeach

    @include('flash.success')
@stop
