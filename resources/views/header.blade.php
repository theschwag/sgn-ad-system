<nav class="navbar navbar-material-teal-800 navbar-fixed-top">
	<div class="container-fluid">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="/"><i class="fa fa-line-chart"></i>&nbsp;{{ config('app.name')  }}</a>
	</div>
	<div id="navbar" class="navbar-collapse collapse">
		<ul class="nav navbar-nav navbar-right">
			@if (!Auth::guest())
			<li class="nobg">
				<span>Welcome <a class="white" href="{{ $app['url']->to('/') }}/users/{{ Auth::user()->id }}/edit/"><span class="bold">{{ Auth::user()->name }}</span></a> </span>
			</li>
			<span class="visible-xs visible-sm">
				<li {{ Request::is('dashboard') ? 'class=active' : '' }}><a class="white" href="{{ url('dashboard') }}"><i class="fa fa-tachometer"></i>&nbsp;&nbsp;Dashboard</a></li>
				@if( Auth::user()->role_id != 1 )
				<li {{ Request::is('messages') ? 'class=active' : '' }}><a class="white"  href="{{ url('messages/create') }}"><i class="fa fa-bullhorn"></i>&nbsp;&nbsp;Post Message</a></li>
				<li {{ Request::is('users') ? 'class=active' : '' }}><a class="white" href="{{ url('users') }}"><i class="fa fa-users"></i>&nbsp;&nbsp;Users</a></li>
				<li {{ Request::is('customers') ? 'class=active' : '' }}><a class="white" href="{{ url('customers') }}"><i class="fa fa-users"></i>&nbsp;&nbsp;Customers</a></li>
				@endif
				<li {{ Request::is('ads') ? 'class=active' : '' }}><a class="white" href="{{ url('ads') }}"><i class="fa fa-money"></i>&nbsp;&nbsp;Ads</a></li>
			</span>
			<li><a href="{{ url('/logout') }}">Logout</a></li>
			@endif
		</ul>
	</div>
	</div>
</nav>
