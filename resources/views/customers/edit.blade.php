@extends('content')

@section('inner')
  <div class="row">
    <div class="col-sm-8">
      {!! Form::model($customer, ['method' => 'PATCH', 'action' => ['CustomerController@update', $customer->id]]) !!}
        @include('customers.form', ['buttonText' => 'Update'])
      {!! Form::close() !!}
    </div>
  </div>
  @include('errors.list')
  @include('flash.success')
@stop

@section('scripts')
<script>
@stop
