{!! Form::text('name', null, ['class' => 'form-control input-lg margin_btm1', 'placeholder' => 'Name of customer (required)']) !!}
{!! Form::text('email', null, ['class' => 'form-control input-lg margin_btm1', 'placeholder' => 'Email Address (required)']) !!}
{!! Form::text('address', null, ['class' => 'form-control input-lg margin_btm1', 'placeholder' => 'Street Address']) !!}
{!! Form::text('city', null, ['class' => 'form-control input-lg margin_btm1', 'placeholder' => 'City']) !!}
{!! Form::text('state', null, ['class' => 'form-control input-lg margin_btm1', 'placeholder' => 'State']) !!}
{!! Form::text('phone', null, ['class' => 'form-control input-lg margin_btm1', 'placeholder' => 'Phone Number']) !!}
{!! Form::button('Submit', ['class' => 'submit-btn btn btn-material-teal-900 btn-lg margin_top1 col-sm-6', 'type'=>'submit']) !!}
