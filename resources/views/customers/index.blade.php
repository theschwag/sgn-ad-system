@extends('content')

@section('buttons')
    <a href="{{ url('customers/create') }}" class="btn btn-raised btn-material-orange-500 btn-md pull-right col-xs-12 col-sm-5"><i
                class="fa fa-plus-circle"></i>&nbsp;&nbsp;New Customer</a>
@stop

@section('inner')
    <div class="table-responsive">
        <table class="table table-striped table-hover">
            <?php //print_r($ads) ?>
            <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>City</th>
                <th>State</th>
                <th class="text-center">Actions</th>
            </tr>
            </thead>
            @foreach($customers as $cust)
            <tr @if($cust->deleted_at != NULL) class="deleted" @endif>
                <td>{{ $cust->name }}</td>
                <td>{{ $cust->email }}</td>
                <td>{{ $cust->phone }}</td>
                <td>{{ $cust->city }}</td>
                <td>{{ $cust->state }}</td>
                @if($cust->deleted_at == NULL)
                <td class="text-center">
                    <button type="button" class="btn btn-info btn-fab margin0 edit"
                            href="customers/{{ $cust->id }}/edit/"><i class="fa fa-pencil"></i></button>
                    &nbsp;
                    {!! Form::open(['method' => 'DELETE', 'id' => 'DeleteCustomer', 'class' => 'inline', 'action' =>
                    ['CustomerController@destroy', $cust->id]]) !!}
                    {!! Form::button( '<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-fab margin0
                    btn-danger delete'] ) !!}
                    {!! Form::close() !!}
                </td>
                @endif
                @if($cust->deleted_at != NULL)
                <td class="text-center">
                    {!! Form::open(['method' => 'RESTORE', 'id' => 'RestoreCustomer', 'class' => 'inline', 'action' =>
                    ['CustomerController@restore', $cust->id]]) !!}
                    {!! Form::button( '<i class="fa fa-undo"></i>', ['type' => 'submit', 'class' => 'btn btn-fab margin0 btn-success
                    restore'] ) !!}
                    {!! Form::close() !!}
                </td>
                @endif
            </tr>
            @endforeach
        </table>
    </div>
    @include('flash.success')
    @include('flash.error')
    @stop

    @section('scripts')
    {!!HTML::script('js/controllers/customer_index.js')!!}
    @stop
