@extends('content')

@section('inner')
  <div class="row">
    <div class="col-sm-8">
      {!! Form::open(['name' => 'customer_create', 'url' => 'customers', 'files' => 'true']) !!}
        @include('customers.form', ['buttonText' => 'Submit'])
      {!! Form::close() !!}
    </div>
  </div>
  @include('errors.list')
  @include('flash.success')
  @include('flash.error')
@stop
