@extends('main')

@section('content')
<div class="container-fluid">
    <div class="row">
        @include('layouts/sidebar')
        <div class="col-xs-12 col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <div class="row">
            <div class="col-sm-6">
              <h1 class="uppercase bold">{{ $heading }}</h1>
            </div>
            <div class="col-sm-6">
              @yield('buttons')
            </div>
          </div>
          <hr>
          @yield('inner')
        </div>
    </div>
</div>
@stop
