<!DOCTYPE html>
<html lang="en">
<head>
	@include('meta')
</head>
<body class="app">
	@include('header')
	@yield('login')
	@yield('content')
	@include('scripts')
</body>
</html>
