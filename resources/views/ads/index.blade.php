@extends('content')

@if(Auth::user()->role_id != 1)
@section('buttons')
    <a href="{{ url('ads/create') }}" class="btn btn-raised btn-material-orange-500 btn-md pull-right col-xs-12 col-sm-5"><i
                class="fa fa-plus-circle"></i>&nbsp;&nbsp;New Ad</a>
@stop
@endif

@section('inner')
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Customer</th>
                <th>Tracked URL</th>
                <!-- <th>Details</th> -->
                <th>Ad Links To</th>
                <th>Start Date</th>
                <th>End Date</th>
                @if(Auth::user()->role_id != 1)
                    <th class="text-center">Actions</th>
                @endif
            </tr>
            </thead>
            @foreach($ads as $ad)
                <tr @if($ad->deleted_at != NULL || $ad->cust_deleted != NULL) class="deleted" @endif>
                    @if($ad->cust_deleted != NULL)
                        <td>{{ $ad->name }} <span class="bold italic">(deleted)</span></td>
                    @endif
                    @if($ad->cust_deleted == NULL)
                        <td>{{ $ad->name }}</td>
                    @endif
                    <td><a href="{{ $ad->tracking_url }}" target="_blank">{{ $ad->tracking_url }}</a></td>
                    <td><a href="{{ $ad->url_to }}" target="_blank">{{ $ad->url_to }}</a></td>
                    <td>
                        <?php
                        $start = Carbon::parse($ad->start_date);
                        echo $start->format('m/d/Y');
                        ?>
                    </td>
                    <td>
                        <?php
                        $end = Carbon::parse($ad->end_date);
                        echo $end->format('m/d/Y');
                        ?>
                    </td>
                    @if(Auth::user()->role_id != 1)
                        @if($ad->deleted_at == NULL)
                            <td class="text-center">
                                <button type="button" class="btn btn-fab btn-info margin0 edit"
                                        href="ads/{{ $ad->id }}/edit/"><i class="fa fa-pencil"></i></button>
                                &nbsp;
                                {!! Form::open(['method' => 'DELETE', 'id' => 'DeleteAd', 'class' => 'inline', 'action' => ['AdController@destroy', $ad->id]]) !!}
                                {!! Form::button( '<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-fab btn-danger margin0 delete'] ) !!}
                                {!! Form::close() !!}
                            </td>
                        @endif
                        @if($ad->deleted_at != NULL)
                            <td class="text-center">
                                {!! Form::open(['method' => 'RESTORE', 'id' => 'RestoreAd', 'class' => 'inline', 'action' => ['AdController@restore', $ad->id]]) !!}
                                {!! Form::button( '<i class="fa fa-undo"></i>', ['type' => 'submit', 'class' => 'btn btn-fab margin0 btn-success restore'] ) !!}
                                {!! Form::close() !!}
                            </td>
                        @endif
                    @endif
                </tr>
            @endforeach
        </table>
    </div>
    @include('flash.success')
    @include('flash.error')
@stop

@section('scripts')
    {!!HTML::script('js/controllers/ad_index.js')!!}
@stop
