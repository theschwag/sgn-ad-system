@section('styles')
  {!!HTML::style('css/bootstrap-datepicker-built.css')!!}
@stop

@extends('content')

@section('inner')
  <div class="row">
    <div class="col-sm-8">
      {!! Form::open(['name' => 'ad_create', 'url' => 'ads', 'files' => 'true']) !!}
        @include('ads.form', ['buttonText' => 'Submit'])
      {!! Form::close() !!}
    </div>
  </div>
  @include('errors.list')
  @include('flash.success')
@stop

@section('scripts')
  {!!HTML::script('js/moment.js')!!}
  {!!HTML::script('js/bootstrap-datepicker.js')!!}
  {!!HTML::script('js/bootstrap.file-input.js')!!}
  {!!HTML::script('js/controllers/ad_create.js')!!}
@stop
