{!! Form::select('customer_id', $customer_list, null, ['class' => 'form-control input-lg margin_btm1']) !!}

{!! Form::text('url_to', null, ['class' => 'form-control input-lg margin_btm1', 'placeholder' => 'URL to link ad to']) !!}

{{--<div class="form-group is-fileinput clearfix">--}}
  {{--<label for="inputFile" class="control-label">File</label>--}}
  {{--<input type="file" id="inputFile" multiple>--}}
  {{--<input type="text" readonly class="form-control input-lg" placeholder="Browse...">--}}
{{--</div>--}}

<input type="file" name="image_link" title="Add Image File" data-filename-placement="inside">

{!! Form::select('position', [
  '0' => 'Choose Position',
  '1' => 'Home/Content/Top (728x90)',
  '2' => 'Home/Content/Bottom (728x90)',
  '3' => 'Sidebar/Small (125x125)',
  '4' => 'Sidebar/Big (300x250)',
  '5' => 'Footer/Featured (300x250)',
  '6' => 'Page/Content/Top (728x90)',
  '7' => 'Page/Content/Bottom (728x90)'
], null, ['class' => 'form-control input-lg margin_top1']) !!}

<?php
if(isset($ad)):
  $start_format = Carbon::parse($ad->start_date)->format('m/d/Y');
  $end_format = Carbon::parse($ad->end_date)->format('m/d/Y');
  $get_start = ($ad->start_date) ? $start_format : null;
  $get_end = ($ad->end_date) ? $end_format : null;
else:
  $get_start = null;
  $get_end = null;
endif;
?>

{!! Form::text('start_date', $get_start, ['class' => 'form-control input-lg margin_top1', 'placeholder' => 'Start Date']) !!}
{!! Form::text('end_date', $get_end, ['class' => 'form-control input-lg margin_top1', 'placeholder' => 'End Date']) !!}

{!! Form::button($buttonText, ['class' => 'submit-btn btn btn-material-teal-900 btn-lg margin_top1 col-sm-6', 'type'=>'submit']) !!}
