<tr id="stats-ad-{{ $ad->id }}" class="hidden">
  <td>
    <div class="detail_wrap">
      <div class="col-sm-4">
        <img class="img-responsive ad_preview" src="images/ads/{{ $ad->ad_file }}">
      </div>
      <div class="col-sm-8">
        Tracking URL: <a href="{{ $ad->tracking_url }}" target="_blank">{{ $ad->tracking_url }}</a></td>
      </div>
    </div>
  </td>
</tr>
