{!! Form::select('role_id', [
  '0' => 'Choose Which Group Sees Message',
  '1' => 'Customers',
  '2' => 'Admins',
  '4' => 'Everyone'
], null, ['class' => 'form-control input-lg margin_top1 margin_btm1']) !!}
{!! Form::textarea('message', null, ['class' => 'form-control input-lg margin_btm1', 'placeholder' => 'Enter Message Here']) !!}
{!! Form::button('Submit', ['class' => 'submit-btn btn btn-material-teal-900 btn-lg margin_top1 col-sm-6', 'type'=>'submit']) !!}
