@extends('content')

@section('inner')
  <div class="row">
    <div class="col-sm-8">
      {!! Form::open(['name' => 'message_create', 'url' => 'messages', 'files' => 'false']) !!}
        @include('messages.form', ['buttonText' => 'Submit'])
      {!! Form::close() !!}
    </div>
  </div>
  @include('errors.list')
  @include('flash.success')
@stop
