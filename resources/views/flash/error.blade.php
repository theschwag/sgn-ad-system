@if(Session::has('error'))
    <div id="toast-container" class="toast-top-center">
        <div class="toast toast-error">
            <div class="toast-message">
                {{ Session::get('error') }}
            </div>
        </div>
    </div>
@endif