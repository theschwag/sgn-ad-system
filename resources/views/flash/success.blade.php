@if(Session::has('message'))
  <div id="toast-container" class="toast-top-center">
    <div class="toast toast-success">
        <div class="toast-message">
          {{ Session::get('message') }}
        </div>
    </div>
  </div>
@endif
