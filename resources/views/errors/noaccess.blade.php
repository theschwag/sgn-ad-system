@extends('content')

@section('inner')
  <div class="row">
    <div class="col-sm-8">
      <p>Sorry! You don't have access to this resource</p>
    </div>
  </div>
@include('errors.list')
@include('flash.success')
@stop

@section('scripts')

@stop
