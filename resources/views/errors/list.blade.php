@if($errors->any())
  <div id="toast-container" class="toast-top-center">
    <div class="toast toast-error">
        <div class="toast-message">
          @foreach($errors->all() as $error)
            {{ $error }}
          @endforeach
        </div>
      </div>
  </div>
@endif
