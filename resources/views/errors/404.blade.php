@extends('content')

@section('inner')
<p>
  The page doesn't exist or you are not authorized to access it.
</p>
@stop
