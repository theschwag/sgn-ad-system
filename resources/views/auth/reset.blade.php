<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name')  }} | Password Reset</title>

    @include('styles')
</head>
<body class="app-login">
<div class="login">
    <div class="container">
        <div class="col-sm-6 col-sm-offset-3 login-area">

            <div class="panel panel-danger hide">
                <div class="panel-heading">
                    <h3 class="panel-title">Error!</h3>
                </div>
                <div class="panel-body">
                </div>
            </div>

            <div class="well well-material-teal-300 clearfix">
                <h2 class="text-center">
                    <i class="fa fa-line-chart"></i>&nbsp;{{ config('app.name')  }}
                </h2>
                <form method="POST" action="/password/reset" class="form form-horizontal margin_top2">
                    {!! csrf_field() !!}
                    <input type="hidden" name="token" value="{{ $token }}">

                    @if (count($errors) > 0)
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    <div>
                        <input type="email" name="email" value="{{ old('email') }}" placeholder="Email" class="form-control input-lg">
                    </div>

                    <div class="margin_top1">
                        <input type="password" name="password" placeholder="Password" class="form-control input-lg">
                    </div>

                    <div class="margin_top1">
                        <input type="password" name="password_confirmation" placeholder="Confirm Password" class="form-control input-lg">
                    </div>

                    <div class="padding_top2">
                        <button type="submit" class="btn btn-material-teal-800 btn-block btn-lg">
                            Reset Password
                        </button>
                    </div>
                </form>
            </div>
            <div class="margin_top2 text-center">
                <a href="/">Back Home</a>
            </div>

            @include('errors.list')
            @include('flash.success')
            @include('flash.error')
        </div>
    </div>
</div>
@include('scripts')
</body>
</html>