<!DOCTYPE html>
<html lang="en">
  <head>
  	@include('meta')
  </head>
  <body class="app-login">
    <div class="login">
      <div class="container">
        <div class="col-sm-6 col-sm-offset-3 login-area">

          <div class="panel panel-danger hide">
            <div class="panel-heading">
                <h3 class="panel-title">Error!</h3>
            </div>
            <div class="panel-body">
            </div>
          </div>

          <div class="well well-material-teal-300 clearfix">
            <h2 class="text-center">
              <i class="fa fa-line-chart"></i>&nbsp;{{ config('app.name')  }}
            </h2>
            {!!Form::open(['url'=>'/login','class'=>'form form-horizontal margin_top2','name'=>'login_form'])!!}
            <div class="form-group margin0">
                <div class="row margin0">
                    {!! Form::text('email',Input::old('email'),['class'=>'form-control input-lg', 'placeholder' => 'Email Address']) !!}
                </div>
            </div>
            <div class="form-group margin0 padding_top1">
                <div class="row margin0">
                    {!! Form::password('password',['class'=>'form-control input-lg', 'placeholder' => 'Password']) !!}
                </div>
            </div>
            <div class="row margin0 padding_top2">
                {!!Form::button('Login',['class'=>'btn btn-material-teal-800 btn-block btn-lg login-btn', 'type'=>'submit'])!!}
            </div>
            {!!Form::close()!!}
          </div>
          <div class="margin_top2 text-center">
            <a href="/password/email">Forgot Password?</a>
          </div>
        </div>
      </div>
    </div>
    @include('errors.list')
    @include('flash.error')
    @include('scripts')
    {!!HTML::script('js/controllers/login.js')!!}
  </body>
</html>
