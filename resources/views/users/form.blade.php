@if(Auth::user()->role_id != 1)
{!! Form::text('name', null, ['class' => 'form-control input-lg margin_btm1', 'placeholder' => 'Name of User (required)']) !!}
{!! Form::text('email', null, ['class' => 'form-control input-lg margin_btm1', 'placeholder' => 'Email Address (required)']) !!}
@endif

@if( Auth::user()->role_id == 2 )
{!! Form::select('role_id', [
  '0' => 'Select User Role',
  '1' => 'Standard',
  '2' => 'Admin'
], null, ['class' => 'form-control input-lg margin_top1 margin_btm1']) !!}
@endif

@if( Auth::user()->role_id == 3 )
  {!! Form::select('role_id', [
    '0' => 'Select User Role',
    '1' => 'Standard',
    '2' => 'Admin',
    '3' => 'Super Admin'
  ], null, ['class' => 'form-control input-lg margin_top1 margin_btm1']) !!}
@endif

{!! Form::text($pw_input_name, null, ['class' => 'form-control input-lg margin_btm1', 'placeholder' => $pw_placeholder]) !!}
{!! Form::button($buttonText, ['class' => 'submit-btn btn btn-material-teal-900 btn-lg margin_top1 col-sm-6', 'type'=>'submit']) !!}
