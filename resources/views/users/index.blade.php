@extends('content')

@section('buttons')
  <a href="{{ url('users/create') }}" class="btn btn-raised btn-material-orange-500 btn-md pull-right col-xs-12 col-sm-5"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;New User</a>
@stop

@section('inner')
<div class="table-responsive">
  <table class="table table-striped table-hover">
    <?php //print_r($ads) ?>
    <thead>
      <tr>
        <th>Name</th>
        <th>Email</th>
        <th>User Type</th>
        <th class="text-center">Actions</th>
      </tr>
    </thead>
    @foreach($users as $user)
      <tr @if($user->deleted_at != NULL) class="deleted" @endif>
        <td>{{ $user->name }}</td>
        <td>{{ $user->email }}</td>
        <td>@if($user->role_id == 1) Standard @elseif($user->role_id == 2) Admin @elseif($user->role_id == 3) Super Admin @endif</td>
        @if($user->deleted_at == NULL)
        <td class="text-center">
          <button type="button" class="btn btn-info btn-fab margin0 edit" href="users/{{ $user->id }}/edit/"><i class="fa fa-pencil"></i></button>
          &nbsp;
          {!! Form::open(['method' => 'DELETE', 'id' => 'DeleteCustomer', 'class' => 'inline', 'action' => ['UserController@destroy', $user->id]]) !!}
            {!! Form::button( '<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-fab margin0 btn-danger delete'] ) !!}
          {!! Form::close() !!}
        </td>
        @endif
        @if($user->deleted_at != NULL)
        <td class="text-center">
          {!! Form::open(['method' => 'RESTORE', 'id' => 'RestoreUser', 'class' => 'inline', 'action' => ['UserController@restore', $user->id]]) !!}
            {!! Form::button( '<i class="fa fa-undo"></i>', ['type' => 'submit', 'class' => 'btn btn-fab margin0 btn-success restore'] ) !!}
          {!! Form::close() !!}        
        </td>
        @endif
      </tr>
    @endforeach
  </table>
</div>
@include('flash.success')
@include('flash.error')
@stop

@section('scripts')
  {!!HTML::script('js/controllers/user_index.js')!!}
@stop
