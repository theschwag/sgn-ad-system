@extends('content')

@section('inner')
  <div class="row">
    <div class="col-sm-8">
      {!! Form::model($user, ['method' => 'PATCH', 'action' => ['UserController@update', $user->id]]) !!}
      <label for="update_password">Change Password</label>
        @include('users.form', ['buttonText' => 'Update', 'pw_input_name' => 'update_password', 'pw_placeholder' => 'New Password'])
      {!! Form::close() !!}
    </div>
  </div>
  @include('errors.list')
  @include('flash.success')
@stop

@section('scripts')
<script>
@stop
