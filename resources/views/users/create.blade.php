@extends('content')

@section('inner')
  <div class="row">
    <div class="col-sm-8">
      {!! Form::open(['name' => 'user_create', 'url' => 'users', 'files' => 'true']) !!}
        @include('users.form', ['buttonText' => 'Submit', 'pw_input_name' => 'password', 'pw_placeholder' => 'Enter New Password'])
      {!! Form::close() !!}
    </div>
  </div>
  @include('errors.list')
  @include('flash.success')
  @include('flash.error')
@stop
