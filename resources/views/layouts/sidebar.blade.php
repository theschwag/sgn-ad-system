<div class="col-sm-3 col-md-2 sidebar padding0">
	<div class="sidebar_btm">
		<ul class="nav nav-sidebar">
			<li {{ Request::is('dashboard') ? 'class=active' : '' }}><a href="{{ url('dashboard') }}"><i class="fa fa-tachometer"></i>&nbsp;&nbsp;Dashboard</a></li>
			@if(Auth::check())
				@if( Auth::user()->role_id != 1 )
				<li {{ Request::is('messages') ? 'class=active' : '' }}><a href="{{ url('messages/create') }}"><i class="fa fa-bullhorn"></i>&nbsp;&nbsp;Post Message</a></li>
				<li {{ Request::is('users') ? 'class=active' : '' }}><a href="{{ url('users') }}"><i class="fa fa-users"></i>&nbsp;&nbsp;Users</a></li>
				<li {{ Request::is('customers') ? 'class=active' : '' }}><a href="{{ url('customers') }}"><i class="fa fa-users"></i>&nbsp;&nbsp;Customers</a></li>
				@endif
			@endif
			<li {{ Request::is('ads') ? 'class=active' : '' }}><a href="{{ url('ads') }}"><i class="fa fa-money"></i>&nbsp;&nbsp;Ads</a></li>
		</ul>
	</div>
</div>
