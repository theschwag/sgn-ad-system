<!DOCTYPE html>
<html lang="en">
<head>
	@include('meta')
</head>
<body>
  <div class="home">
    <div class="headerbg">
      <div class="container">
        <div class="col-sm-6 col-sm-offset-3 margin_top2 text-center">
          {!!HTML::image('images/scoops_logo.png', '', array('class' => 'img-responsive padding1 center-block logo'))!!}
          <h2>{{ config('app.name')  }}</h2>
          <div class="margin_top1 padding2">
            <a class="btn btn-orange btn-block btn-lg" href="{{ url('/login') }}">Login</a>
          </div>
        </div>
      </div>
    </div>
  </div>
	@include('scripts')
</body>
</html>
