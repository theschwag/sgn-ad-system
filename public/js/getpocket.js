function getPocket() {
    var url = 'http://apis.peteschwager.com/pocketauth/api.php';
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        complete: function(data){
            var obj = data.responseText;
            var clean = $.parseJSON(obj);
            var objects = $.makeArray(clean);
            var newObj = $.parseJSON(objects);
            $.each(newObj.list, function(index, articles){

                the_title = articles.resolved_title; // title of article
                the_url = articles.resolved_url; // link to article
                the_excerpt = articles.excerpt;
                the_time = articles.time_added;

                // put articles into there own list items
                var items = '<div class="col-xs-12 col-sm-4 item" data-time="'+ the_time +'">'+
                                '<div class="panel panel-default panel-pocket padding0">'+
                                    '<div class="panel-heading"><a class="pocket" href="'+ the_url +'" target="_blank">'+ the_title + '</a></div>'+
                                    '<div class="panel-body">'+ the_excerpt +'</div>'+
                                '</div>'+
                            '</div>';

                    // don't show items that don't have a title
                    if(the_title == ''){
                        items = '';
                    }

                //add items to page
                $('.pocketlist').append(items);
                // make the grid be fluid
            });

            // sort by time added. Show newest first
            tinysort('div.pocketlist>div.item', {attr:'data-time', order: 'desc'});

            // build the masonry layout
            var $container = $('.masonry');
            $container.masonry({
                itemSelector: '.item'
            });
        },
    });
}
