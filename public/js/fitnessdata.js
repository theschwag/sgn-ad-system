function getFitness() {
    var url = 'http://apis.peteschwager.com/getdata.php';
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        beforeSend: function(){
            $('.loading').append('<div class="load">'+
                                    '<span class="l-1"></span>'+
                                    '<span class="l-2"></span>'+
                                    '<span class="l-3"></span>'+
                                    '<span class="l-4"></span>'+
                                    '<span class="l-5"></span>'+
                                    '<span class="l-6"></span></div>');
        },
        success: function(data){
            $('.load').remove();
            localStorage.setItem('rides', JSON.stringify(data));
            localStorage.setItem('cachedTime', new Date().getTime());
        },
        error: function( xhr, status, errorThrown ) {
            console.log( 'Error: ' + errorThrown );
            console.log( 'Status: ' + status );
            console.dir( xhr );
        },
        complete: function(){
            storedFitness();
        }
    });
}

function newTime(time) {
    var minutes = Math.floor(time / 60);
    var seconds = time % 60;
    return minutes +':'+ seconds;
}

function storedFitness(){
    ride = JSON.parse(localStorage.getItem('rides'));
    $.each(ride[0], function(index, item){
        // set vars related to my rides
        var miles = (item.distance * 0.00062137119).toFixed(2);
        var yr = item.date.year;
        var year = yr.toString().split('20').pop();
        var ocurrence = item.date.month+'/'+item.date.day+'/'+year;
        var duration = item.duration;
        var activity = item.type;
        var speed = item.speed;
        var weather = item.weather;
        var cal = item.kcal;
        if(activity == 'mountainbiking'){
            var activity = 'fa fa-bicycle';
        }

        // put ride info into seperate boxes
        var items = '<tr>'+
        '<td>'+ ocurrence +'</td>'+
        '<td class="text-center"><i class="'+ activity +' font26"></i></td>'+
        '<td>'+ weather +'</td>'+
        '<td>'+ duration +'</td>'+
        '<td><input type="hidden" class="miles" value="'+ miles +'">'+ miles +'</td>'+
        '<td>'+ speed +'</td>'+
        '<td><input type="hidden" class="calories" value="'+ cal +'">'+ cal +'</td>'+
        '</div>'+
        '</tr>';

        // add each ride event to the DOM
        $('.stats-table').append(items);
    });

    var nbr_rides = $('table tbody tr').length;
    $('.rides').prepend(nbr_rides);

    //show miles rode
    function showMiles() {
        var sum = 0;
        $('.miles').each(function() {
            sum += +this.value;
        });
        return sum.toFixed(2);
    }
    $('.mileage').prepend(showMiles());

    // show cals burned
    function showCals() {
        var sum = 0;
        $('.calories').each(function() {
            sum += +this.value;
        });
        return sum;
    }
    $('.calorie_count').prepend(showCals());

    // call dataTable
    $('.stats-table').dataTable({
        "order": [[ 1, "desc" ]],
        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]]
    });
    // change select box size
    $('select[name="DataTables_Table_0_length"]').switchClass('input-sm', 'input-lg');
}
