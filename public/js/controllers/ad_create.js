//$(document).on('change', '.btn-file :file', function() {
//    var input = $(this),
//        numFiles = input.get(0).files ? input.get(0).files.length : 1,
//        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
//    input.trigger('fileselect', [numFiles, label]);
//});
$(function () {
    // bootstrap uploader
    $('input[type=file]').bootstrapFileInput();
    $('a.file-input-wrapper').addClass('btn-material-orange-500 btn-raised');
    //Datepicker
    var today = moment().format('L');
    $('input[name=start_date]').val(today);
    $('input[name=start_date], input[name=end_date]').datepicker();
});