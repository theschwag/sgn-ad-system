$(function() {
    toastr.options = {
        "positionClass": "toast-top-center",
        "progressBar": true,
        "preventDuplicates": true,
        "showDuration": "180",
    };
    var loginform = $('form[name="login_form"]');
    loginform.submit(function(e) {
        var formdata = loginform.serialize();
        $.ajax({
                type: 'POST',
                url: base_url + '/login',
                dateType: 'json',
                data: formdata,
                beforeSend: function() {
                    $('.login-btn').append('<span class="spinner"><i class="fa fa-circle-o-notch fa-spin"></i></span>');
                }
            })
            .done(function(response) {
                if (response.message == 'success') {
                    toastr.success('Logging You In...');
                    $(location).attr('href', base_url + '/dashboard');
                } else {
                    // show error on credentials if wrong login
                    toastr.error(response.message);
                }
                $('.spinner').remove();
            })
            .error(function(response) {
                var resp = response.responseText;
                var error = $.parseJSON(resp);
                //console.log(error);
                toastr.error((error.email !== undefined) ? error.email : '' + '<br>' + (error.password !== undefined) ? error.password : '');
                $('.spinner').remove();
            });
        e.preventDefault();
    });
});
