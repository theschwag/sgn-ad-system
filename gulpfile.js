var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('main.scss');
    mix.copy('components/bootstrap-datepicker/bootstrap-datepicker-built.css', 'public/css');
    mix.copy('components/bootstrap-datepicker/js/bootstrap-datepicker.js', 'public/js');
});
